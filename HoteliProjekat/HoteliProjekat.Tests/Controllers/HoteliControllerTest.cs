﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using HoteliProjekat.Interfaces;
using HoteliProjekat.Models;
using HoteliProjekat.Controllers;
using System.Web.Http;
using System.Web.Http.Results;
using System.Collections.Generic;
using System.Linq;

namespace HoteliProjekat.Tests.Controllers
{
    [TestClass]
    public class HoteliControllerTest
    {
        [TestMethod]
        public void GetReturnsHotelWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<IHotelRepository>();
            mockRepository.Setup(x => x.GetById(42)).Returns(new Hotel { Id = 42 });

            var controller = new HoteliController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetById(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Hotel>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.Id);
        }

        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRepository = new Mock<IHotelRepository>();
            var controller = new HoteliController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.Put(10, new Hotel { Id = 9, Naziv="Hotel 1" });

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }

        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            // Arrange
            List<Hotel> hoteli = new List<Hotel>();
            hoteli.Add(new Hotel { Id = 1, Naziv = "Hotel1", GodinaOsnivanja = 1990});
            hoteli.Add(new Hotel { Id = 2, Naziv = "Hotel2", GodinaOsnivanja = 2000 });

            var mockRepository = new Mock<IHotelRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(hoteli.AsEnumerable());
            var controller = new HoteliController(mockRepository.Object);

            // Act
            IEnumerable<Hotel> result = controller.GetAll();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(hoteli.Count, result.ToList().Count);
            Assert.AreEqual(hoteli.ElementAt(0), result.ElementAt(0));
            Assert.AreEqual(hoteli.ElementAt(1), result.ElementAt(1));
        }

        [TestMethod]
        public void PostReturnsMultipleObjects()
        {
            // Arrange
            List<Hotel> hoteli = new List<Hotel>();
            hoteli.Add(new Hotel { Id = 1, Naziv = "Hotel1" ,BrojSoba = 100 });
            hoteli.Add(new Hotel { Id = 2, Naziv = "Hotel2" ,BrojSoba = 50 });

            var filter = new Filter() { Najmanje = 30, Najvise = 150 };

            var mockRepository = new Mock<IHotelRepository>();
            mockRepository.Setup(x => x.Kapacitet(filter)).Returns(hoteli.AsEnumerable());
            var controller = new HoteliController(mockRepository.Object);

            // Act
            IEnumerable<Hotel> result = controller.PostKapacitet(filter);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(hoteli.Count, result.ToList().Count);
        
        }
    }
}
