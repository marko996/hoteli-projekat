﻿using HoteliProjekat.Interfaces;
using HoteliProjekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace HoteliProjekat.Controllers
{
    public class LanciController : ApiController
    {
        ILanacRepository _repository { get; set; }

        public LanciController(ILanacRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Lanac> GetAll()
        {
            return _repository.GetAll();
        }
        [ResponseType(typeof(Lanac))]
        public IHttpActionResult GetById(int id)
        {
            var lanac = _repository.GetById(id);

            if(lanac == null)
            {
                return NotFound();
            }

            return Ok(lanac);
        }
        [Authorize]
        [Route("api/tradicija")]
        public IEnumerable<Lanac> GetTradicija()
        {
            return _repository.Tradicija();
        }
        [Route("api/zaposleni")]
        public IEnumerable<LanacZaposleniDTO> GetStatistika()
        {
            return _repository.ProsekZaposlenih();
        }
        [Route("api/sobe")]
        public IEnumerable<Lanac> PostSobe(GranicaClass granica)
        {
            return _repository.Sobe(granica);
        }

    }
}
