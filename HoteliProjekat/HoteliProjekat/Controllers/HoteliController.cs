﻿using HoteliProjekat.Interfaces;
using HoteliProjekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace HoteliProjekat.Controllers
{
    public class HoteliController : ApiController
    {
        IHotelRepository _repository { get; set; }

        public HoteliController(IHotelRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<Hotel> GetAll()
        {
            return _repository.GetAll();
        }
        [ResponseType(typeof(Hotel))]
        public IHttpActionResult GetById(int id)
        {
            var hotel = _repository.GetById(id);

            if(hotel == null)
            {
                return NotFound();
            }

            return Ok(hotel);
        }
        public IEnumerable<Hotel> GetByZaposleni(int zaposleni)
        {
            return _repository.GetByZaposleni(zaposleni);
        }
        [ResponseType(typeof(Hotel))]
        public IHttpActionResult Put(int id, Hotel hotel)
        {
            if (hotel.Id != id)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                _repository.Update(hotel);
            }
            catch
            {
                return BadRequest();
            }

            return Ok(_repository.GetById(hotel.Id));
        }

        [Authorize]
        [ResponseType(typeof(Hotel))]
        public IHttpActionResult Delete(int id)
        {
            var hotel = _repository.GetById(id);

            if (hotel == null)
            {
                return NotFound();
            }

            _repository.Delete(hotel);

            return Ok();
        }

        [Authorize]
        [ResponseType(typeof(Hotel))]
        public IHttpActionResult Post(Hotel hotel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Create(hotel);

            return CreatedAtRoute("DefaultApi", new { id = hotel.Id }, _repository.GetById(hotel.Id));
        }
        [Authorize]
        [Route("api/kapacitet")]
        public IEnumerable<Hotel> PostKapacitet(Filter filter)
        {
            return _repository.Kapacitet(filter);
        }
    }
}
