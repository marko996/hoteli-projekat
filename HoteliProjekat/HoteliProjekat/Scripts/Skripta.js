﻿$(document).ready(function () {
    var host = "http://" + window.location.host;
    var token = null;
    var headers = {};
    var lanciEndpoint = "/api/Lanci";
    var hoteliEndpoint = "/api/Hoteli";

    //priprema dogadjaja
    $("body").on("click", "#btnDelete", deleteHotel);

    //ucitavanje hotela
    loadHoteli();

    //funckija ucitavanja hotela
    function loadHoteli() {
        var requestUrl = host + hoteliEndpoint;
        $.getJSON(requestUrl, setHoteli);
    }

    // metoda za postavljanje hotela u tabelu
    function setHoteli(data, status) {

        var $container = $("#data");
        $container.empty();

        if (status === "success") {
            // ispis naslova
            var div = $("<div style='text-align:center'></div>");
            var h1 = $("<h1>Hoteli</h1>");
            div.append(h1);
            // ispis tabele
            var table = $("<table class='table table-bordered'></table>");
            var header;
            if (token) {
                header = $("<thead style='background-color:yellow'><tr><td>Naziv</td><td>Godina otvaranja</td><td>Broj soba</td><td>Broj zaposleni</td><td>Lanac</td><td>Akcija</td></tr></thead>");
            } else {
                header = $("<thead style='background-color:yellow'><tr><td>Naziv</td><td>Godina otvaranja</td><td>Broj soba</td><td>Broj zaposleni</td><td>Lanac</td></tr></thead>");
            }

            table.append(header);
            tbody = $("<tbody></tbody>");
            for (i = 0; i < data.length; i++) {
                // prikazujemo novi red u tabeli
                var row = "<tr>";
                // prikaz podataka
                var displayData = "<td>" + data[i].Naziv + "</td><td>" + data[i].GodinaOsnivanja + "</td><td>" + data[i].BrojSoba + "</td><td>" + data[i].BrojZaposlenih + "</td><td>" + data[i].Lanac.Naziv + "</td>";
                // prikaz dugmadi za izmenu i brisanje
                var stringId = data[i].Id.toString();
                var displayDelete = "<td><button class='btn btn-default' id=btnDelete name=" + stringId + ">Brisanje</button></td>";

                // prikaz samo ako je korisnik prijavljen
                if (token) {
                    row += displayData + displayDelete + "</tr>";
                } else {
                    row += displayData + "</tr>";
                }
                // dodati red
                tbody.append(row);
            }
            table.append(tbody);

            div.append(table);

            $container.append(div);

        }
    }

    $("#regBtn").click(function () {
        $("#registracija").css("display", "block");
        $("#prijava").css("display", "none");
        $("#priBtn").css("display", "none");
        $("#regBtn").css("display", "none");
    });

    $("#priBtn").click(function () {
        $("#prijava").css("display", "block");
        $("#registracija").css("display", "none");
        $("#priBtn").css("display", "none");
        $("#regBtn").css("display", "none");
    });

    $("#odustajanje1").click(function (e) {
        e.preventDefault();
        $("#prijava").css("display", "none");
        $("#registracija").css("display", "none");
        $("#priBtn").css("display", "block");
        $("#regBtn").css("display", "block");
    });

    $("#odustajanje2").click(function (e) {
        e.preventDefault();
        $("#prijava").css("display", "none");
        $("#registracija").css("display", "none");
        $("#priBtn").css("display", "block");
        $("#regBtn").css("display", "block");
    });

   

    // registracija korisnika
    $("#registracija").submit(function (e) {
        e.preventDefault();

        var email = $("#regEmail").val();
        var loz1 = $("#regLoz").val();
        var loz2 = $("#regLoz2").val();

        // objekat koji se salje
        var sendData = {
            "Email": email,
            "Password": loz1,
            "ConfirmPassword": loz2
        };

        $.ajax({
            type: "POST",
            url: host + "/api/Account/Register",
            data: sendData

        }).done(function (data) {
            $("#info2").css("display","block");
            $("#regEmail").val("");
            $("#regLoz").val("");
            $("#regLoz2").val("");

        }).fail(function (data) {
            alert("Greska prilikom registracije!");
        });
    });

    $("#priSistem").click(function () {
        $("#prijava").css("display", "block");
        $("#registracija").css("display", "none");
        $("#info2").css("display", "none");
    });


    // prijava korisnika
    $("#prijava").submit(function (e) {
        e.preventDefault();

        var email = $("#priEmail").val();
        var loz = $("#priLoz").val();

        // objekat koji se salje
        var sendData = {
            "grant_type": "password",
            "username": email,
            "password": loz
        };

        $.ajax({
            "type": "POST",
            "url": host + "/Token",
            "data": sendData

        }).done(function (data) {
            $("#info").empty().append("Prijavljen korisnik: <strong>" + data.userName +"</strong>");
            token = data.access_token;
            $("#prijava").css("display", "none");
            $("#registracija").css("display", "none");
            $("#priBtn").css("display", "none");
            $("#regBtn").css("display", "none");
            $("#pretraga").css("display", "block");
            $("#dodavanje").css("display", "block");
            $("#tradicija").css("display","block");
            $("#odjava").css("display", "block");
            $("#priEmail").val("");
            $("#priLoz").val("");
            loadHoteli();

        }).fail(function (data) {
            alert("Greska prilikom prijave!");
        });
    });

    // odjava korisnika sa sistema
    $("#odjava").click(function () {
        token = null;
        headers = {};

        location.reload();


        loadHoteli();

    });

    //brisanje hotela
    function deleteHotel() {

        var deleteId = this.name;

        if (token) {
            headers.Authorization = "Bearer " + token;
        }

        $.ajax({
            url: host + hoteliEndpoint + "/" + deleteId.toString(),
            type: "DELETE",
            headers: headers
        })
            .done(function (data, status) {
                loadHoteli();
            })
            .fail(function (data, status) {
                alert("Doslo je do greske!");
            });
    }

    //pretraga hotela
    $("#pretraga").submit(function (e) {
        e.preventDefault();

        var start = $("#start").val();
        var kraj = $("#kraj").val();

        if (isNaN(start) || isNaN(kraj)) {
            alert("Nevalidan unos!")
            return false;
        }

        if (token) {
            headers.Authorization = "Bearer " + token;
        }

        var sendObject = {
            "Najmanje": start,
            "Najvise": kraj
        };

        $.ajax({
            url: host + "/api/kapacitet",
            type: "POST",
            headers: headers,
            data: sendObject
        })
            .done(function (data, status) {
                $("#start").val("");
                $("#kraj").val("");
                setHoteli(data, status);
            })
            .fail(function (data, status) {
                alert("Greska prilikom pretrage!");
            });

    });

    //popunjavanje selekta
    $.ajax({
        "type": "GET",
        "url": host + lanciEndpoint
    }).done(function (data, status) {
        var select = $("#lanac");
        for (var i = 0; i < data.length; i++) {
            var option = "<option value='" + data[i].Id + "'>" + data[i].Naziv + "</option>";
            select.append(option);
        }
    });

    //dodavanje hotela
    $("#dodavanje").submit(function (e) {
        e.preventDefault();

        if (token) {
            headers.Authorization = "Bearer " + token;
        }
        var lanac = $("#lanac").val();
        var naziv = $("#naziv").val();
        var godina = $("#godina").val();
        var sobe = $("#sobe").val();
        var zaposleni = $("#zaposleni").val();

        var sendObject = {
            "Naziv": naziv,
            "GodinaOsnivanja": godina,
            "BrojSoba": sobe,
            "BrojZaposlenih": zaposleni,
            "LanacId": lanac
        };


        $.ajax({
            type: "POST",
            url: host + hoteliEndpoint,
            headers: headers,
            data: sendObject
        })
            .done(function (data, status) {
                $("#naziv").val("");
                $("#godina").val("");
                $("#sobe").val("");
                $("#zaposleni").val("");
                $("#lanac").val("1");

                loadHoteli();
            })
            .fail(function (data, status) {
                alert("Greska prilikom dodavanja!");
            });
    });

    //na klik odustajanje
    $("#odustajanje").click(function (e) {
        e.preventDefault();

        $("#naziv").val("");
        $("#godina").val("");
        $("#sobe").val("");
        $("#zaposleni").val("");
        $("#lanac").val("1");
    });

    //ucitavanje tradicije
    $("#ucitavanje").click(function () {

        $("#ucitavanje").css("display","none");

        if (token) {
            headers.Authorization = "Bearer " + token;
        }

        $.ajax({
            type: "GET",
            url: host + "/api/tradicija",
            headers: headers
        })
            .done(function (data, status) {
                if (data[0] !== null) {
                    $("#tradicijaDiv").append("<br/> <p> <strong> 1. " + data[0].Naziv + " </strong> (osnovan <strong>" + data[0].GodinaOsnivanja + "</strong> godine) </p>");
                }

                if (data[1] !== null) {
                    $("#tradicijaDiv").append("<p> <strong> 2. " + data[1].Naziv + " </strong> (osnovan <strong>" + data[1].GodinaOsnivanja + "</strong> godine) </p>");
                }
                
            });

    });


});