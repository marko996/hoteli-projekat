﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HoteliProjekat.Models
{
    public class LanacSobeDTO
    {
        public Lanac Lanac { get; set; }
        public int? Sobe { get; set; }
    }
}