﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HoteliProjekat.Models
{
    public class LanacZaposleniDTO
    {
        public string Lanac { get; set; }
        public double ProsekZaposlenih { get; set; }
    }
}