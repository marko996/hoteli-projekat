﻿using HoteliProjekat.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HoteliProjekat.Models;

namespace HoteliProjekat.Repository
{
    public class LanacRepository : IDisposable,ILanacRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Lanac> GetAll()
        {
            return db.Lanci;
        }

        public Lanac GetById(int id)
        {
            return db.Lanci.FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Lanac> Tradicija()
        {
            var lanci = db.Lanci.OrderBy(x => x.GodinaOsnivanja).ToList();

            var najstariji = new List<Lanac>();

            if(lanci[0] != null)
            {
                najstariji.Add(lanci[0]);
            }

            if (lanci[1] != null)
            {
                najstariji.Add(lanci[1]);
            }

            return najstariji;

        }

        public IEnumerable<LanacZaposleniDTO> ProsekZaposlenih()
        {
            return db.Hoteli.GroupBy(x => x.Lanac, x => x.BrojZaposlenih, (lanac, zaposleni) => new LanacZaposleniDTO() { Lanac = lanac.Naziv,ProsekZaposlenih = zaposleni.Average()}).OrderByDescending(x => x.ProsekZaposlenih);


        }

        public IEnumerable<Lanac> Sobe(GranicaClass granica)
        {
            var lanci = db.Hoteli.GroupBy(x => x.Lanac,x => x.BrojSoba,(lanac,sobe) => new LanacSobeDTO() { Lanac = lanac,Sobe = sobe.Sum()} ).OrderBy(x => x.Sobe);

            var lanci1 = new List<Lanac>();

            foreach(LanacSobeDTO l in lanci)
            {
                if(l.Sobe > granica.Granica)
                {
                    lanci1.Add(l.Lanac);
                }
            }

            return lanci1;
        }
    }
}