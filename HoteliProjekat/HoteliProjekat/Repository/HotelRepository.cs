﻿using HoteliProjekat.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HoteliProjekat.Models;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace HoteliProjekat.Repository
{
    public class HotelRepository : IDisposable,IHotelRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Hotel> GetAll()
        {
            return db.Hoteli.Include(x => x.Lanac).OrderBy(x => x.GodinaOsnivanja);
        }

        public Hotel GetById(int id)
        {
            return db.Hoteli.Include(x => x.Lanac).FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Hotel> GetByZaposleni(int zaposleni)
        {
            return db.Hoteli.Include(x => x.Lanac).Where(x => x.BrojZaposlenih >= zaposleni).OrderBy(x => x.BrojZaposlenih);
        }

        public void Create(Hotel hotel)
        {
            db.Hoteli.Add(hotel);

            db.SaveChanges();
        }

        public void Update(Hotel hotel)
        {
            db.Entry(hotel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(Hotel hotel)
        {
            db.Hoteli.Remove(hotel);

            db.SaveChanges();
        }

        public IEnumerable<Hotel> Kapacitet(Filter filter)
        {
            return db.Hoteli.Include(x => x.Lanac).Where(x => x.BrojSoba <= filter.Najvise && x.BrojSoba >= filter.Najmanje).OrderByDescending(x => x.BrojSoba);
        }
    }
}