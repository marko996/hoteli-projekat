namespace HoteliProjekat.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MadeBrojSobaattributenullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Hotels", "BrojSoba", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Hotels", "BrojSoba", c => c.Int(nullable: false));
        }
    }
}
