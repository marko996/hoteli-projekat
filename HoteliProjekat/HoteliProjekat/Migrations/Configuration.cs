namespace HoteliProjekat.Migrations
{
    using HoteliProjekat.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<HoteliProjekat.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(HoteliProjekat.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Lanci.AddOrUpdate(
                new Lanac() { Naziv = "Hilton Worldwide",GodinaOsnivanja=1919},
                new Lanac() { Naziv = "Marriott International", GodinaOsnivanja = 1927 },
                new Lanac() { Naziv = "Kempinski", GodinaOsnivanja = 1897 }
                );

            context.SaveChanges();

            context.Hoteli.AddOrUpdate(
                new Hotel() { Naziv = "Sheraton Novi Sad",GodinaOsnivanja=2018,BrojZaposlenih=70,BrojSoba=150,LanacId=2},
                new Hotel() { Naziv = "Hilton Belgrade", GodinaOsnivanja = 2017, BrojZaposlenih = 100, BrojSoba = 242, LanacId = 1 },
                new Hotel() { Naziv = "Palais Hansen", GodinaOsnivanja = 2013, BrojZaposlenih = 80, BrojSoba = 150, LanacId = 3 },
                new Hotel() { Naziv = "Budapest Marriott", GodinaOsnivanja = 1994, BrojZaposlenih = 130, BrojSoba = 364, LanacId = 2 },
                new Hotel() { Naziv = "Hilton Berlin", GodinaOsnivanja = 1991, BrojZaposlenih = 200, BrojSoba = 601, LanacId = 1 }
                );

            context.SaveChanges();
        }
    }
}
