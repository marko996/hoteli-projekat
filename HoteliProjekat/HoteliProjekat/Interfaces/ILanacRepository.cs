﻿using HoteliProjekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoteliProjekat.Interfaces
{
    public interface ILanacRepository
    {
        IEnumerable<Lanac> GetAll();
        Lanac GetById(int id);
        IEnumerable<Lanac> Tradicija();
        IEnumerable<LanacZaposleniDTO> ProsekZaposlenih();
        IEnumerable<Lanac> Sobe(GranicaClass granica);
    }
}
