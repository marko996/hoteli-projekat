﻿using HoteliProjekat.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HoteliProjekat.Interfaces
{
    public interface IHotelRepository
    {
        IEnumerable<Hotel> GetAll();
        Hotel GetById(int id);
        IEnumerable<Hotel> GetByZaposleni(int zaposleni);
        void Create(Hotel hotel);
        void Update(Hotel hotel);
        void Delete(Hotel hotel);
        IEnumerable<Hotel> Kapacitet(Filter filter);
    }
}
